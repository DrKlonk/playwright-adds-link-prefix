import { test, expect } from "@playwright/test"

test("Use tab to navigate buttons and links", async ({ page }) => {
  await page.goto("/")

  await expect(page.getByTestId("anchor-tag")).toBeVisible()
  await expect(page.getByTestId("anchor-tag")).toHaveAttribute(
    "href",
    "https://google.nl" // actually "link://https://google.nl" when inspected
  )

  await page.getByTestId("button1").focus()

  await page.keyboard.press("Tab")

  await expect(page.getByTestId("anchor-tag")).toBeFocused() // on chrome, firefox and mobile chrome
  // await expect(page.getByTestId("button2")).toBeFocused() // on webkit and mobile safari
})
